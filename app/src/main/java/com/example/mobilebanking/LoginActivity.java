package com.example.mobilebanking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private AccountDAO accountDAO;
    private EditText Email;
    private EditText Password;
    private String valueEmail = null;
    private String valuePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);

        accountDAO = new Login(this);

        Email = (EditText) findViewById(R.id.email);
        Password = (EditText) findViewById(R.id.password);
    }

    public void SubmitLogin(View view) {
        valuePassword = Password.getText().toString();
        valueEmail = Email.getText().toString();

        if (valueEmail.trim().length() == 0){
            Toast.makeText(this, "Nhập tên đăng nhập"+valueEmail, Toast.LENGTH_SHORT).show();
            return;
        }else if(valuePassword.trim().length() == 0){
            Toast.makeText(this, "Nhập mật khẩu", Toast.LENGTH_SHORT).show();
            return;
        } else{
            Account account = accountDAO.Login(valueEmail,valuePassword);
            if (account != null){
                Toast.makeText(this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                intent.putExtra("loggedFullname",account.getFullname());
                intent.putExtra("loggedMoney",account.getMoney());
                startActivity(intent);
            }else{
                Toast.makeText(this, "Sai tên đăng nhập/mật khẩu", Toast.LENGTH_SHORT).show();
            }
        }
    }
}