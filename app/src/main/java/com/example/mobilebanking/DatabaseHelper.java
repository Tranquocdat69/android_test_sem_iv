package com.example.mobilebanking;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
private static final String DATABASE_NAME = "banking.sglite";
private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE tblAccount (\n" +
                "    id           INTEGER      PRIMARY KEY AUTOINCREMENT,\n" +
                "    Fullname     VARCHAR (64) NOT NULL,\n" +
                "    Passport_num VARCHAR (32) NOT NULL,\n" +
                "    Email        VARCHAR (32) NOT NULL\n" +
                "                              UNIQUE,\n" +
                "    Password     VARCHAR (16) NOT NULL,\n" +
                "    Money        DOUBLE       NOT NULL\n" +
                "                              DEFAULT (50000) \n" +
                ");";
        sqLiteDatabase.execSQL(sql);

            sql = "INSERT INTO tblAccount(Fullname,Passport_num,Email,Password,Money) VALUES('Trần Quốc Đạt','0123456789','admin','123',45678.9)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
