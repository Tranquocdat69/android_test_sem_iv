package com.example.mobilebanking;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    private TextView currentLoggedFullname;
    private TextView currentLoggedMoney;

    private String loggedFullname;
    private String loggedMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        currentLoggedFullname = findViewById(R.id.currentLoggedFullname);
        currentLoggedMoney = findViewById(R.id.currentLoggedMoney);

        loggedFullname = getIntent().getExtras().getString("loggedFullname");

        double moneyDouble = getIntent().getExtras().getDouble("loggedMoney");

        NumberFormat nf = NumberFormat.getInstance();
        String parse = nf.format(moneyDouble);
            loggedMoney = String.valueOf(parse)+" đ";
            currentLoggedFullname.setText(loggedFullname);
            currentLoggedMoney.setText(loggedMoney);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

}