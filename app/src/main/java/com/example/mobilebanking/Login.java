package com.example.mobilebanking;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Login implements AccountDAO{
    private Context context;
    private SQLiteDatabase db;

    public Login(Context context) {
        this.context = context;
        DatabaseHelper databaseHelper =new DatabaseHelper(this.context);
        this.db = databaseHelper.getWritableDatabase();
    }

    @Override
    public Account Login(String Email, String Password) {
        String sql = "select * from tblAccount where Email = ?1 and Password = ?2";
        String[] params = {Email +"",Password+""};
        Cursor cursor =db.rawQuery(sql,params);

        Account account = null;
        while (cursor.moveToNext()){
            int indexId = cursor.getColumnIndex("id");
            int indexFullname = cursor.getColumnIndex("Fullname");
            int indexEmail = cursor.getColumnIndex("Email");
            int indexMoney = cursor.getColumnIndex("Money");

            account = new Account();

            account.setId(cursor.getInt(indexId));
            account.setFullname(cursor.getString(indexFullname));
            account.setEmail(cursor.getString(indexEmail));
            account.setMoney(cursor.getDouble(indexMoney));
        }
        return account;
    }
}
